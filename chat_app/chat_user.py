from typing import Dict


class User:

    msg_index: Dict[str, int]

    def __init__(self,
                 name,
                 group='default'):
        self.name = name
        self.group = group
        self.msg_index = {}

    def __str__(self):
        return f"User(name='{self.name}', group='{self.group}')"
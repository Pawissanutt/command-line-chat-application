import json
import string


class ChatMessage:
    index: int = -1

    def __init__(self,
                 group: string,
                 owner: string,
                 text: string,
                 file_name: string=None,
                 file_path: string=None,
                 size: int = -1):
        self.group = group
        self.owner = owner
        self.text = text
        self.file_name = file_name
        self.file_path = file_path
        self.size = size

    def __str__(self):
        return f"Message(group='{self.group}', owner='{self.owner}', text='{self.text}', " \
               f"file_name={self.file_name}, file_path={self.file_path}, size={self.size})"

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__)
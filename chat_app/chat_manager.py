import logging
import os
import string
import random
from typing import Dict, Any

from .chat_log import ChatLog, ChatMessage
from .chat_user import User

logger = logging.getLogger(__name__)


class ChatManager:

    def __init__(self):
        self.log = ChatLog()
        self.sessions = SessionManager(self)
        self.users: Dict[str, User] = {}

    def pub_msg(self, msg: ChatMessage):
        self.log.add_new(msg)
        for conn, session in self.sessions.session_map.items():
            if session.user.group == msg.group:
                session.send(msg)

    def get_or_create_user(self, name, group):
        if name in self.users:
            return self.users[name]
        user = User(name, group)
        self.users[name] = user
        return user


class Session:

    def __init__(self,
                 user: User,
                 manager: ChatManager,
                 conn):
        self.user = user
        self.conn = conn
        self.manager = manager
        self.upload_file = None
        self.upload_remain = 0

    def switch_group(self, new_group):
        index = -1
        if new_group in self.user.msg_index:
            index = self.user.msg_index[new_group]
        else:
            self.user.msg_index[new_group] = index
        self.user.group = new_group
        log = self.manager.log.get_or_create_group_log(new_group)
        log = log[index + 1:]
        for msg in log:
            self.send(msg)

    def send(self, msg: ChatMessage):
        payload = msg.to_json().encode()
        size = len(payload)
        self.conn.sendall(size.to_bytes(4, 'big') + payload)
        self.user.msg_index[msg.group] = msg.index

    def handle(self, req):
        if 'cmd' not in req:
            return
        cmd = req['cmd']
        if cmd == 'send':
            txt = req['txt']
            msg = ChatMessage(group=self.user.group,
                              owner=self.user.name,
                              text=txt)
            logger.info(f"receive message {msg}")
            self.manager.pub_msg(msg)
        if cmd == 'load':
            if 'index' not in req:
                index = 0
            else:
                index = int(req['index'])
            msgs = self.manager.log.groups[self.user.group][index:]
            for msg in msgs:
                self.send(msg)

        if cmd == 'switch':
            if 'group' not in req:
                return
            new_group = req['group']
            self.switch_group(new_group)

        if cmd == 'upload':
            self.upload_remain = int(req['size'])
            new_file_name = 'files/' + ''.join(random.choice(string.ascii_lowercase) for i in range(8))
            msg = ChatMessage(group=self.user.group,
                              owner=self.user.name,
                              text=None,
                              file_name=req['name'],
                              file_path=new_file_name,
                              size=self.upload_remain)
            logger.info(f"receive file upload {msg}")
            self.upload_file = open(new_file_name, 'wb')
            self.manager.pub_msg(msg)

        if cmd == 'download':
            index = req['index']
            msg = self.manager.log.get_or_create_group_log(self.user.group)[index]
            if msg.file_path is None:
                return
            self.send(msg)
            with open(msg.file_path, 'rb') as file:
                while True:
                    bytes_read = file.read(1024)
                    if not bytes_read:
                        break
                    self.conn.send(bytes_read)

    def __str__(self):
        return f"Session(user='{self.user}')"


class SessionManager:
    session_map: Dict[Any, Session] = {}

    def __init__(self,
                 manager: ChatManager):
        self.manager = manager

    def add_session(self,
                    conn,
                    req):
        user_name = req['name']
        group = 'default'
        user = self.manager.get_or_create_user(user_name, group)
        session = Session(user, self.manager, conn)
        self.session_map[conn] = session
        logger.info(f'create new {session}')
        session.switch_group(user.group)

    def remove_session(self, conn):
        session = self.session_map.pop(conn)
        logger.info(f'delete {session}')

    def get_session(self, conn) -> Session:
        if conn in self.session_map:
            return self.session_map[conn]
        return None

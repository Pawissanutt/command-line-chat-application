import json
import string

from typing import List, Dict

from .chat_message import ChatMessage


class ChatLog:
    groups: Dict[str, List[ChatMessage]]

    def __init__(self):
        self.groups = {}

    def add_new(self, msg: ChatMessage):
        if msg.group in self.groups:
            msg.index = len(self.groups[msg.group])
            self.groups[msg.group].append(msg)
        else:
            msg.index = 0
            self.groups[msg.group] = [msg]

    def get_or_create_group_log(self, group: str):
        if group not in self.groups:
            self.groups[group] = []
        return self.groups[group]

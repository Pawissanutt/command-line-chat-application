#!/usr/bin/env python3

import json
import selectors
import socket
import ssl
import logging
from json import JSONDecodeError

from chat_app import ChatManager

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

HOST = 'localhost'
PORT = 1234

sel = selectors.DefaultSelector()
manager = ChatManager()


def parse_req(txt):
    if len(txt) < 2:
        return
    try:
        return json.loads(txt)
    except JSONDecodeError:
        logging.exception(f"json decode error!! {txt}")


def handle_msg(conn, payload):
    req = parse_req(payload)
    if req is None:
        return
    session = manager.sessions.get_session(conn)
    if session is None:
        if 'name' in req and 'cmd' in req:
            manager.sessions.add_session(conn, req)
    else:
        session.handle(req)


def accept(sock, mask):
    try:
        conn, addr = sock.accept()  # Should be ready
        logger.info(f'accepted {conn} from {addr}')
        conn.setblocking(False)
        sel.register(conn, selectors.EVENT_READ, read)
    except ssl.SSLError:
        logger.warning("Client cannot connect to server because ssl error")


def read(conn, mask):
    data = conn.recv(16384)  # Should be ready
    session = manager.sessions.get_session(conn)
    if data:
        i = 0
        while i < len(data):
            if session is None or session.upload_file is None:
                size = int.from_bytes(data[i:i + 4], 'big')
                i = i + 4
                payload = data[i: i + size]
                handle_msg(conn, payload)
                i += size
            else:
                segment = data[i:session.upload_remain]
                session.upload_file.write(segment)
                session.upload_remain -= len(segment)
                if session.upload_remain <= 0:
                    session.upload_file.close()
                    session.upload_file = None
                i += len(segment)

    else:
        logger.info(f'closing {conn}')
        sel.unregister(conn)
        manager.sessions.remove_session(conn)
        conn.close()


if __name__ == "__main__":
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.load_cert_chain('certificate.pem', 'key.pem')
    sock = socket.socket()
    sock.bind((HOST, PORT))
    sock.listen(100)
    sock.setblocking(False)
    ssock = context.wrap_socket(sock, server_side=True)
    sel.register(ssock, selectors.EVENT_READ, accept)

    while True:
        events = sel.select()
        for key, mask in events:
            callback = key.data
            callback(key.fileobj, mask)

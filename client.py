#!/usr/bin/env python3

import json
import os
import select
import socket
import ssl
import sys

HOST = 'localhost'
PORT = 1234
BUFFER_SIZE=16384

download_file = None
download_remain = -1
download_index = -1

def send_json(socket,
              msg):
    payload = bytes(json.dumps(msg), 'utf8')
    size = len(payload)
    socket.sendall(size.to_bytes(4, 'big') + payload)


def handle_stdin(socket):
    global download_file
    global download_index
    txt = stdin.readline().rstrip()
    # remove last line
    if txt.startswith('/'):
        args = txt.split(' ')
        if txt == '/exit':
            sys.exit()
        elif txt.startswith('/help'):
            print('"/help" to show available commands')
            print('"/exit" to exit the program')
            print('"/load <index>" to load the old message starting from <index> in current group')
            print('"/switch <group>" to switch to another group')
            print('"/upload <file_path>" to upload the file to server')
            print('"/download <index> <file_path>" to download the file from given message index '
                  'into the given file path')
        elif txt.startswith('/load'):
            if len(args) < 2:
                print("Command error")
                return
            try:
                index = int(args[1])
            except Exception:
                print("Command error")
                return
            msg = {'cmd': 'load', 'index': index}
            send_json(socket, msg)
        elif txt.startswith('/switch'):
            if len(args) < 2:
                print("Command error")
                return
            msg = {'cmd': 'switch', 'group': args[1]}
            send_json(socket, msg)
        elif txt.startswith('/upload'):
            if len(args) < 2:
                print("Command error")
                return
            file_path = args[1]
            file_size = os.path.getsize(file_path)
            with open(file_path, 'rb') as file:
                msg = {'cmd': 'upload', 'name': file.name, 'size': file_size}
                send_json(socket, msg)
                while True:
                    bytes_read = file.read(BUFFER_SIZE)
                    if not bytes_read:
                        break
                    socket.send(bytes_read)
        elif txt.startswith('/download'):
            if len(args) < 3:
                print("Command error")
                return
            download_index = int(args[1])
            msg = {'cmd': 'download', 'index': download_index}
            send_json(socket, msg)
            download_file = open(args[2], 'wb')

    else:
        print("\033[A                             \033[A")
        msg = {'cmd': 'send', 'txt': txt}
        send_json(socket, msg)


def handle_socket(socket):
    global download_remain
    global download_file
    try:
        data = socket.recv(BUFFER_SIZE)
    except ssl.SSLWantReadError:
        return
    i = 0
    while i < len(data):
        if download_remain <= 0:
            size = int.from_bytes(data[i:i + 4], 'big')
            i = i + 4
            payload = data[i: i + size]
            msg = json.loads(payload)
            i += size
            if msg['index'] == download_index:
                download_remain = msg['size']
                continue
            owner = msg['owner']
            if owner == name:
                owner = 'you'
            if msg['file_name'] is None:
                print(f"[{msg['group']}] {owner}\t=({msg['index']})>{msg['text']}")
            else:
                print(f"[{msg['group']}] {owner}\t=({msg['index']})----File(name='{msg['file_name']}', "
                      f"size={msg['size']})----")
        else:
            segment = data[i: download_remain]
            download_file.write(segment)
            download_remain -= len(segment)
            if download_remain <= 0:
                download_file.close()
                download_file = None
                print("=== Download Completed ===")
            i += len(segment)


if __name__ == "__main__":

    context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    context.load_verify_locations('certificate.pem')
    context.check_hostname = False

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket:
        with context.wrap_socket(socket,
                                 server_hostname=HOST) as ssock:
            print(f"Connecting to server {HOST}:{PORT}")
            ssock.connect((HOST, PORT))
            ssock.setblocking(False)
            name = input('Enter your name: ')

            msg = {'cmd': 'init', 'name': name}
            send_json(ssock, msg)

            stdin = sys.stdin
            inputs = [ssock, stdin]
            while True:
                readableList, writableList, exceptionalList = select.select(inputs, [], [])
                for readable in readableList:
                    if readable == stdin:
                        handle_stdin(ssock)
                    else:
                        handle_socket(ssock)
